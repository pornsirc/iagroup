﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Citizen
    {
        public string citizen_id { get; set; }
    }
    public class Citizen_out
    {
        public bool Success { get; set; }
        public string Error_code { get; set; }
        public string Error_msg { get; set; }
    }
}
