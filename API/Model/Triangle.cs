﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Triangle
    {
        public int Base { get; set; }
        public int height { get; set; }
    }
    public class Triangle_out
    {
        public double Area { get; set; }
    }
}
