﻿using API.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TriangleController : Controller
    {
        [HttpPost]
        public JsonResult Post([FromBody] Triangle model)
        {
            int tbase = model.Base;
            int theight = model.height;
            Triangle_out Result = new Triangle_out();
            Result.Area = 0.5 * tbase * theight;
            return Json(Result);
        }
    }
}
