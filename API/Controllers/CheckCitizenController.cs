﻿using API.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckCitizenController : Controller
    {
        [HttpPost]
        public JsonResult Post([FromBody] Citizen model)
        {
            string citizen_id = model.citizen_id;         
            Citizen_out Result = new Citizen_out();
           
            if(citizen_id == "")
            {
                 Result.Success = false;
                 Result.Error_code = "001";
                 Result.Error_msg = "citizen_id require";
            }
            else
            {
                var arr = Split(citizen_id,1);
                var res = arr.ToList();
                if (res.Count != 12)
                {
                    Result.Success = false;
                    Result.Error_code = "001";
                    Result.Error_msg = "citizen_id invalid";
                }
                else
                {
                    int sum = 0;
                    int l = 13;
                    for (int i = 0; i < res.Count; i++)
                    {
                        
                        int re = l * Convert.ToInt32(res[i]);
                        sum += re;
                        l--;
                    }
                    string Validator = (11 - (sum % 11)).ToString();

                    if (Validator == "11") Validator = "1";
                    else if (Validator == "10") Validator = "0";

                    Result.Success = true;
                    Result.Error_code = "200";
                    Result.Error_msg = Validator;
                }
            }
            

            return Json(Result);
        }
        public static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}
