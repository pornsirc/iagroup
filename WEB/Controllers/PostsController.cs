﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WEB.Models;

namespace WEB.Controllers
{
    public class PostsController : Controller
    {
        // GET: Posts
        public ActionResult Index()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync("https://jsonplaceholder.typicode.com/posts").Result;
            ListPosts dt = new ListPosts();
            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                dt.list = new List<Posts>();
                dt.list = JsonConvert.DeserializeObject<List<Posts>>(json);
             
            }
            return View(dt);
        }
    }
}